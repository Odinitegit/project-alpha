from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.models import User
from .forms import AccountLoginForm, SignUpForm

def login_view(request):
    
    if request.method == "POST":
        form = AccountLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username = username,
                password = password,
            )
            if user is not None:
                login(request,user)
                return redirect('list_projects')
            
        return render(request, 'accounts/login.html', {'form': form})
        
    else:

        form = AccountLoginForm()
        context = {
            'form':form,
        }

        return render(request,'accounts/login.html',context )
   
def user_logout(request):
    logout(request)
    return redirect('login')

def signup_view(request):

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password = password,
                )

                login(request,user)
                return redirect('list_projects')
            else:
                form.add_error("password","Passwords do not Match.")
    else:
        
        form = SignUpForm()
        
    context = {
        'form': form,
    }

        
    return render(request,'accounts/registration/signup.html',context)
      
