from django.shortcuts import render,redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import CreateProject


# Create your views here.

@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {'projects':projects}
    return render(request,'projects.html',context) 

@login_required
def project_detail(request, id):
    projects = Project.objects.get(id=id)
    context = {
        'projects': projects
    }

    return render(request, 'detail.html',context)

@login_required
def create_project(request):
    if request.method == 'POST':
        form = CreateProject(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list_projects')  
    else:
        form = CreateProject()
        
        context = {
            'form': form
            }

    return render(request, 'create_project.html', context)

